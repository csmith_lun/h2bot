# Copyright (C) 2021 The Hunter2 Contributors.
#
# This file is part of Hunter2 Bot.
#
# Hunter2 Bot is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 Bot is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.


import os

import discord
import re
import requests

from fuzzywuzzy import process as fuzzy
from discord.ext import commands

# Assumptions:
# Team names don't change (not currently supported by H2, but would result in "old" role names)

DISCORD_ADMIN_ROLE = os.getenv('H2_DISCORD_ADMIN_ROLE') or "discord-admin"
ADMIN_ROLE = os.getenv('H2_ADMIN_ROLE') or "hunt-admin"
EXTRA_ROLES = os.getenv('H2_EXTRA_ROLES').split(",") if os.getenv('H2_EXTRA_ROLES') else []
ROLES_TO_CREATE = [DISCORD_ADMIN_ROLE, ADMIN_ROLE] + EXTRA_ROLES

MEME_CHANNEL = os.getenv('H2_MEME_CHANNEL') or "\U0001F921│memes"
ADMIN_MEME_CHANNEL = os.getenv('H2_ADMIN_MEME_CHANNEL') or "admin-meme"
ADMIN_STUCK_CHANNEL = os.getenv('H2_ADMIN_STUCK_CHANNEL') or "admin-stuck"
ADMIN_FEEDBACK_CHANNEL = os.getenv('H2_ADMIN_FEEDBACK_CHANNEL') or "admin-feedback"

PUBLIC_CATEGORY = os.getenv('H2_PUBLIC_CATEGORY') or "Public"
EXTRA_PUBLIC_CHANNELS = os.getenv('H2_EXTRA_PUBLIC_CHANNELS').split(",") if os.getenv('H2_EXTRA_PUBLIC_CHANNELS') else []
# Public channels will auto-censor "sensitive" commands
PUBLIC_CHANNELS = ["\U0001F4AC│general", "\U0001F93C│looking-for-team", "\U0001F192│advertising"] + EXTRA_PUBLIC_CHANNELS
EXTRA_PUBLIC_VOICE_CHANNELS = os.getenv('H2_EXTRA_PUBLIC_VOICE_CHANNELS').split(",") if os.getenv('H2_EXTRA_PUBLIC_VOICE_CHANNELS') else []
PUBLIC_VOICE_CHANNELS = ["Public Voice"] + EXTRA_PUBLIC_VOICE_CHANNELS
EXTRA_ANNOUNCE_CHANNELS = os.getenv('H2_EXTRA_ANNOUNCE_CHANNELS').split(",") if os.getenv('H2_EXTRA_ANNOUNCE_CHANNELS') else []
ANNOUNCE_CHANNELS = ["\U0001F44B│welcome", "\U0001F4E2│announcements", MEME_CHANNEL] + EXTRA_ANNOUNCE_CHANNELS

ADMIN_CATEGORY = os.getenv('H2_ADMIN_CATEGORY') or "admins"
EXTRA_ADMIN_CHANNELS = os.getenv('H2_EXTRA_ADMIN_CHANNELS').split(",") if os.getenv('H2_EXTRA_ADMIN_CHANNELS') else []
ADMIN_CHANNELS = ["hunt-admin-general", "hunt-admin-hunter2", "hunt-admin-puzzle-design",
                  "hunt-admin-finance", "hunt-admin-merch",
                  ADMIN_MEME_CHANNEL, ADMIN_STUCK_CHANNEL, ADMIN_FEEDBACK_CHANNEL] + EXTRA_ADMIN_CHANNELS
EXTRA_ADMIN_VOICE_CHANNELS = os.getenv('H2_EXTRA_ADMIN_VOICE_CHANNELS').split(",") if os.getenv('H2_EXTRA_ADMIN_VOICE_CHANNELS') else []
ADMIN_VOICE_CHANNELS = ["hunt-admin-voice"] + EXTRA_ADMIN_VOICE_CHANNELS

# This defines the finishers channels created
EPISODES = os.getenv('H2_EPISODES').split(",") if os.getenv('H2_EPISODES') else ["\U0001F389│episode1", "\U0001F973│episode2"]

EXTRA_CATEGORIES = os.getenv('H2_EXTRA_CATEGORIES').split(",") if os.getenv('H2_EXTRA_CATEGORIES') else []

PERMANENT_CHANNELS = ADMIN_CHANNELS + ANNOUNCE_CHANNELS + PUBLIC_CHANNELS
PERMANENT_VOICE_CHANNELS = ADMIN_VOICE_CHANNELS + PUBLIC_VOICE_CHANNELS
PERMANENT_CATEGORIES = [ADMIN_CATEGORY, PUBLIC_CATEGORY] + EXTRA_CATEGORIES
PERMANENT_ROLES = ROLES_TO_CREATE + ["@everyone"]

FEEDBACK_LINK = os.getenv('H2_FEEDBACK_LINK')

HUNTER2_TOKEN = os.getenv('H2_API_TOKEN')
HUNTER2_URL = os.getenv('H2_API_URL').strip('/')
TOKEN = os.getenv('H2_DISCORD_TOKEN')

intents = discord.Intents(messages=True, members=True, guilds=True, dm_messages=True, guild_messages=True, guild_reactions=True, reactions=True)
hc = commands.DefaultHelpCommand(width=1000, no_category="Commands")
bot = commands.Bot(command_prefix='!', description="Your friendly puzzle hunt bot", intents=intents, help_command=hc)

# Import all plugins
if os.path.isdir('./plugins'):
    for filename in os.listdir('./plugins'):
        if filename.endswith('.py'):
            bot.load_extension(f'plugins.{filename[:-3]}')


@bot.event
async def on_ready():
    print(f'Logged in as {bot.user} (ID: {bot.user.id})')
    print('------')


@bot.command(name='feedback', help="Provide general feedback (use !stuck for anything puzzle specific)")
async def feedback(ctx, *feedback_words):
    feedback = ' '.join(feedback_words)
    guild = ctx.guild or bot.guilds[0]
    message = ctx.message
    user = message.author
    member = await guild.fetch_member(user.id)
    print("INFO: Feedback from {}".format(user.name))

    if not feedback:
        feedback_text = ("Feedback link is: {}".format(FEEDBACK_LINK) if FEEDBACK_LINK else
                         "The feedback form is not available yet - please check back later or DM an admin")
        await user.send(content="Your feedback is welcome! " + feedback_text)
        return
    feedbackchan = list(chan for chan in guild.channels if chan.name == ADMIN_FEEDBACK_CHANNEL)[0]

    member_roles = list(role for role in member.roles if role.name.startswith("team-"))

    team_text = ""
    guessed_teamchat = ""

    if member_roles:
        team_text = "from team '{}' ".format(member_roles[0].name[5:])
        guessed_teamchat = "teamchat-{}".format(member_roles[0].name[5:].lower().replace(" ", "-"))
        # [5:] will strip "team-" from the beginning of a string

    if message.guild:
        # If a user !feedbacks in a team channel, link directly to the message
        reply_link = message.jump_url
    else:
        # If a user !feedbacks in a DM, guess the team channel
        channels = guild.channels
        reply_link = ""
        for chan in channels:
            if chan.name == guessed_teamchat:
                reply_link = chan.mention

    await feedbackchan.send(content="User {} {}sends feedback: {}. {}".format(user.mention, team_text, feedback[:1500], reply_link))

    response_message = "Thank you, {}. Your feedback is appreciated!".format(user)
    if await censorMessageIfPublic(ctx):
        await user.send(response_message)
    else:
        await ctx.send(response_message)


@bot.command(name='stuck', help="Informs the admins you are stuck on a puzzle. Please only use this in a PM with the bot or a team chat")
async def stuck(ctx, *problem_words):
    problem = ' '.join(problem_words)
    guild = ctx.guild or bot.guilds[0]
    message = ctx.message
    user = message.author
    member = await guild.fetch_member(user.id)
    print("INFO: Stuck from {}".format(user.name))

    if not problem:
        problem = "No problem specified"
    stuckchan = list(chan for chan in guild.channels if chan.name == ADMIN_STUCK_CHANNEL)[0]

    member_roles = list(role for role in member.roles if role.name.startswith("team-"))

    team_text = ""
    guessed_teamchat = ""

    if member_roles:
        team_text = "from team '{}' ".format(member_roles[0].name[5:])
        guessed_teamchat = "teamchat-{}".format(member_roles[0].name[5:].lower().replace(" ", "-"))
        # [5:] will strip "team-" from the beginning of a string

    if message.guild:
        # If a user !stucks in a team channel, link directly to the message
        reply_link = message.jump_url
    else:
        # If a user !stucks in a DM, guess the team channel
        channels = guild.channels
        reply_link = ""
        for chan in channels:
            if chan.name == guessed_teamchat:
                reply_link = chan.mention

    await stuckchan.send(content="User {} {}is stuck with message: {}. {}".format(user.mention, team_text, problem[:1500], reply_link))

    response_message = "Thank you, {}. The admins have been informed of your woes".format(user)
    if await censorMessageIfPublic(ctx):
        await user.send(response_message)
    else:
        await ctx.send(response_message)


@bot.command(name='jointeam',
             help="Join your team chats with the ID found on the 'team' page. Please only use this in a PM with the bot",
             aliases=['join', 'joinTeam'])
async def jointeam(ctx, teamid):
    guild = ctx.guild or bot.guilds[0]
    user = ctx.message.author
    member = await guild.fetch_member(user.id)
    print("INFO: Join team from {}".format(user.name))

    await censorMessageIfPublic(ctx)

    member_roles = list(role for role in member.roles if role.name.startswith("team-"))
    if member_roles:
        await user.send(content="You already have joined a discord team channel. Please contact the admins to change team")
        print("INFO: User {} already joined to team with ID {}".format(user.name, teamid))
        return

    url = '{}/hunt/teaminfo/{}'.format(HUNTER2_URL, teamid)
    headers = {'Authorization': "Bearer " + HUNTER2_TOKEN}
    try:
        response = requests.get(url, headers=headers)
    except OSError:
        print("OSError encountered while joining team. Is the hunter2 instance available from the bot host?")
        await user.send(content="There was an error joining your team. Please contact the hunt admins")
        return

    if response.status_code == 404:
        print("WARN: Unable to find team with ID {}".format(teamid))
        await user.send(
            content="There was an error joining your team. "
            "Please double check the code you fetched from the 'Team' page, and if the problem persists, contact the hunt admins"
        )
        return
    if response.status_code != 200:
        print("WARN: HTTP {} received while trying to join {} to {}".format(str(response.status_code), user.name, teamid))
        await user.send(content="There was an error joining your team. Please contact the hunt admins")
        return
    teamname = response.json()['team']['name']

    teamrole = await createRoleIfNotExists("team-{}".format(teamname)[:100], guild)
    adminrole = getRoleByName(ADMIN_ROLE, guild)
    botrole = guild.me.roles[1]
    # The bot needs permissions to see into the created channels.
    # This assumes the bot only has one role, as it is hard to determine the "bot" role (this could be a config option in future)

    permissions = {
            guild.default_role: discord.PermissionOverwrite(read_messages=False),
            teamrole: discord.PermissionOverwrite(read_messages=True),
            botrole: discord.PermissionOverwrite(read_messages=True),
            adminrole: discord.PermissionOverwrite(read_messages=True)
            }

    category = await createCategoryIfNotExists("Team: {}".format(teamname)[:100], permissions, guild)
    textchan = await createTextChannelIfNotExists("teamchat-{}".format(teamname)[:100], category, guild)
    voicechan = await createVoiceChannelIfNotExists("teamvoice-{}".format(teamname)[:100], category, guild)

    await member.add_roles(teamrole)
    await user.send(content="You have been joined to your team channels: {} and {}".format(textchan.mention, voicechan.mention))


@bot.command(name='meme', help="Submit a meme for approval. Please only use this in a PM with the bot or a team chat", aliases=['memes'])
async def meme(ctx):
    print("INFO: Meme command")
    guild = ctx.guild or bot.guilds[0]
    message = ctx.message
    user = ctx.message.author

    memechan = getTextChannelByName(ADMIN_MEME_CHANNEL, guild)

    files = []
    for attachment in message.attachments:
        fl = await attachment.to_file()
        files.append(fl)
    memecontent = re.sub(r'\!memes?', '', message.content)
    message = await memechan.send(content="Submitted by {}. {}".format(user.mention, memecontent), files=files)
    await censorMessageIfPublic(ctx)
    await user.send(content="Your meme has been submitted")

    await message.add_reaction('\N{THUMBS UP SIGN}')
    for i in range(0, len(EPISODES)):
        await message.add_reaction(u"{}\N{VARIATION SELECTOR-16}\N{COMBINING ENCLOSING KEYCAP}".format(i+1))
    await message.add_reaction('\N{THUMBS DOWN SIGN}')


@bot.command(name='serversetup', help="(Admin Only) Setup the required admin channels and permissions", hidden=True)
async def serversetup(ctx):
    print("INFO: Server Setup command")
    guild = ctx.guild or bot.guilds[0]
    user = ctx.message.author
    if type(user) != discord.Member:
        await user.send(content="Please ensure this is run from within the discord server you wish to configure")
        return
    if not await checkMemberRole(user, [DISCORD_ADMIN_ROLE], guild) and user != guild.owner:
        print("WARN: Permission denied to {} trying to run serversetup command".format(user.name))
        await ctx.send("You need to be server owner or admin to use this command")
        return

    for role in ROLES_TO_CREATE:
        await createRoleIfNotExists(role, guild)

    adminrole = getRoleByName(ADMIN_ROLE, guild)
    botrole = guild.me.roles[1]

# PUBLIC OBJECTS
    public_permissions = {
            guild.default_role: discord.PermissionOverwrite(read_messages=True, mention_everyone=False),
            botrole: discord.PermissionOverwrite(read_messages=True, send_messages=True),
            adminrole: discord.PermissionOverwrite(read_messages=True)
            }
    publiccat = await createCategoryIfNotExists(PUBLIC_CATEGORY, public_permissions, guild)
    for chan in PUBLIC_CHANNELS:
        await createTextChannelIfNotExists(chan, publiccat, guild, stripSpecial=False)

    for chan in PUBLIC_VOICE_CHANNELS:
        await createVoiceChannelIfNotExists(chan, publiccat, guild)

# ANNOUNCE OBJECTS
    announce_permissions = {
            guild.default_role: discord.PermissionOverwrite(read_messages=True, send_messages=False),
            botrole: discord.PermissionOverwrite(read_messages=True, send_messages=True),
            adminrole: discord.PermissionOverwrite(read_messages=True)
            }
    for chan in ANNOUNCE_CHANNELS:
        await createTextChannelIfNotExists(chan, publiccat, guild, permissions=announce_permissions, stripSpecial=False)


# ADMIN OBJECTS
    admin_permissions = {
            guild.default_role: discord.PermissionOverwrite(read_messages=False),
            botrole: discord.PermissionOverwrite(read_messages=True, send_messages=True),
            adminrole: discord.PermissionOverwrite(read_messages=True)
            }
    admincat = await createCategoryIfNotExists(ADMIN_CATEGORY, admin_permissions, guild)

    for chan in ADMIN_CHANNELS:
        await createTextChannelIfNotExists(chan, admincat, guild, stripSpecial=False)

    for chan in ADMIN_VOICE_CHANNELS:
        await createVoiceChannelIfNotExists(chan, admincat, guild)

# FINISHERS OBJECTS
    for episode in EPISODES:
        role = await createRoleIfNotExists("{}-finishers".format(episode), guild)

        finish_permissions = {
                guild.default_role: discord.PermissionOverwrite(read_messages=False),
                role: discord.PermissionOverwrite(read_messages=True, mention_everyone=False),
                botrole: discord.PermissionOverwrite(read_messages=True, send_messages=True),
                adminrole: discord.PermissionOverwrite(read_messages=True)
                }
        finishmeme_permissions = {
                guild.default_role: discord.PermissionOverwrite(read_messages=False),
                role: discord.PermissionOverwrite(read_messages=True, send_messages=False),
                botrole: discord.PermissionOverwrite(read_messages=True),
                adminrole: discord.PermissionOverwrite(read_messages=True)
                }
        finishcat = await createCategoryIfNotExists(episode, finish_permissions, guild)

        await createTextChannelIfNotExists("{}-finishers".format(episode), finishcat, guild, stripSpecial=False)
        await createTextChannelIfNotExists("{}-memes".format(episode), finishcat, guild, permissions=finishmeme_permissions, stripSpecial=False)
        await createVoiceChannelIfNotExists("{}-finishers".format(episode), finishcat, guild)


@bot.command(name='springclean', help="(Admin Only) Remove all team/finishers channels, categories and roles", hidden=True)
async def springclean(ctx, confirmation=0):
    print("INFO: Springclean command")
    guild = ctx.guild or bot.guilds[0]
    user = ctx.message.author

    if type(user) != discord.Member:
        await user.send(content="Please ensure this is run from within the discord server you wish to configure")
        return
    if not await checkMemberRole(user, [DISCORD_ADMIN_ROLE], guild) and user != guild.owner:
        print("WARN: Permission denied to {} trying to run serversetup command".format(user.name))
        await ctx.send("You need to be server owner or admin to use this command")
        return

    for channel in guild.text_channels:
        if channel.name not in PERMANENT_CHANNELS:
            if confirmation == guild.id:
                print("DELETE CHANNEL: " + channel.name)
                await user.send("DELETE CHANNEL: " + channel.name)
                await channel.delete()
            else:
                print("WOULD DELETE CHANNEL: " + channel.name)
                await user.send("WOULD DELETE CHANNEL: " + channel.name)
    for voice_channel in guild.voice_channels:
        if voice_channel.name not in PERMANENT_VOICE_CHANNELS:
            if confirmation == guild.id:
                print("DELETE VOICE CHANNEL: " + voice_channel.name)
                await user.send("DELETE VOICE CHANNEL: " + voice_channel.name)
                await voice_channel.delete()
            else:
                print("WOULD DELETE VOICE CHANNEL: " + voice_channel.name)
                await user.send("WOULD DELETE VOICE CHANNEL: " + voice_channel.name)
    for category in guild.categories:
        if category.name not in PERMANENT_CATEGORIES:
            if confirmation == guild.id:
                print("DELETE CATEGORY: " + category.name)
                await user.send("DELETE CATEGORY: " + category.name)
                await category.delete()
            else:
                print("WOULD DELETE CATEGORY: " + category.name)
                await user.send("WOULD DELETE CATEGORY: " + category.name)

    myroles = [role.name for role in guild.me.roles]
    for role in guild.roles:
        if role.name not in PERMANENT_ROLES + myroles:
            if confirmation == guild.id:
                print("DELETE ROLE: " + role.name)
                await user.send("DELETE ROLE: " + role.name)
                await role.delete()
            else:
                print("WOULD DELETE ROLE: " + role.name)
                await user.send("WOULD DELETE ROLE: " + role.name)

    if confirmation == guild.id:
        await serversetup(ctx)


@bot.command(name='teamsearch', help="(Admin Only) Allows the admins to search for a team's chat", aliases=["ts"], hidden=True)
async def teamsearch(ctx, *teamarray):
    teamname = ' '.join(teamarray)
    guild = ctx.guild or bot.guilds[0]
    message = ctx.message
    user = message.author

    if await censorMessageIfPublic(ctx):
        return
    print("INFO: Team search for {} from {}".format(teamname, user.name))

    if not await checkMemberRole(user, [ADMIN_ROLE], guild) and user != guild.owner:
        print("WARN: Permission denied to {} trying to run teamsearch command".format(user.name))
        await ctx.send("You need to be server owner or admin to use this command")
        return

    categoryList = []
    for category in guild.categories:
        if "Team: " in category.name:
            categoryList.append(category.name[6:])

    catname = fuzzy.extractOne(teamname, categoryList)[0]
    catobj = getCategoryByName("Team: " + catname, guild)
    tc = catobj.text_channels[0]
    vc = catobj.voice_channels[0]
    await ctx.send(tc.mention)
    await ctx.send(vc.mention)


async def createRoleIfNotExists(role, guild, force=False):
    create = force
    try:
        role_obj = getRoleByName(role, guild)
    except ValueError:
        create = True

    if create:
        print("INFO: creating role {}".format(role))
        role_obj = await guild.create_role(name=role, hoist=True)
    return role_obj


async def createCategoryIfNotExists(category, permissions, guild, force=False):
    create = force
    try:
        cat_obj = getCategoryByName(category, guild)
    except ValueError:
        create = True

    if create:
        print("INFO: creating category {}".format(category))
        cat_obj = await guild.create_category(name=category, overwrites=permissions)
    return cat_obj


async def createTextChannelIfNotExists(channel, category, guild, permissions=None, force=False, stripSpecial=True):
    if stripSpecial:
        channel = channel.lower().replace(" ", "-")
        channel = re.sub(r'[^a-zA-Z0-9_-]', '', channel)

    create = force

    try:
        chan_obj = getTextChannelByName(channel, guild)
        if chan_obj.category != category:
            create = True
    except ValueError:
        create = True

    if create:
        print("INFO: creating text channel {}".format(channel))
        if permissions:
            chan_obj = await guild.create_text_channel(channel, category=category, overwrites=permissions)
        else:
            chan_obj = await guild.create_text_channel(channel, category=category)

    return chan_obj


async def createVoiceChannelIfNotExists(channel, category, guild, force=False):
    create = force
    try:
        chan_obj = getVoiceChannelByName(channel, guild)
    except ValueError:
        create = True

    if create:
        print("INFO: creating voice channel {}".format(channel))
        chan_obj = await guild.create_voice_channel(channel, category=category)
    return chan_obj


def getRoleByName(role, guild):
    roleObjects = list(s_role for s_role in guild.roles if s_role.name == role)

    if len(roleObjects) != 1:
        raise ValueError("Search for role did not return exactly 1 result")

    return roleObjects[0]


def getCategoryByName(category, guild):
    catObjects = list(cat for cat in guild.categories if cat.name == category)

    if len(catObjects) != 1:
        raise ValueError("Search for text channel did not return exactly 1 result")

    return catObjects[0]


def getTextChannelByName(channel, guild):
    chanObjects = list(chan for chan in guild.text_channels if chan.name == channel)

    if len(chanObjects) != 1:
        raise ValueError("Search for text channel did not return exactly 1 result")

    return chanObjects[0]


def getVoiceChannelByName(channel, guild):
    chanObjects = list(chan for chan in guild.voice_channels if chan.name == channel)

    if len(chanObjects) != 1:
        raise ValueError("Search for voice channel did not return exactly 1 result")

    return chanObjects[0]


async def checkMemberRole(memberObject, acceptedRoles, guild):
    if type(memberObject) == discord.User:
        memberObject = await guild.fetch_member(memberObject.id)

    for role in memberObject.roles:
        if role.name in acceptedRoles:
            return True
    return False

# Meme approval via reactions
@bot.event
async def on_raw_reaction_add(payload):
    channel = bot.get_channel(payload.channel_id)
    guild = bot.get_guild(payload.guild_id)
    user = bot.get_user(payload.user_id)
    emoji = payload.emoji

    if not guild:  # Ignore DMs
        return

    memeadminchan = getTextChannelByName(ADMIN_MEME_CHANNEL, guild)
    message = await channel.fetch_message(payload.message_id)
    if user == guild.me:  # Ignore the auto-populated emoji for memes
        return
    if message.channel != memeadminchan:  # Only watch meme admin channel
        return
    if guild.me != message.author:  # Ensure it's a meme submitted through the correct method
        return

    reaction = list(reac for reac in message.reactions if str(reac.emoji) == str(emoji))[0]

    if reaction.count != 2:
        return

    success = 0
    if str(emoji) == u"\N{THUMBS UP SIGN}":
        print("INFO: Meme approved")
        memechan = getTextChannelByName(MEME_CHANNEL, guild)
        files = []
        for attachment in message.attachments:
            fl = await attachment.to_file()
            files.append(fl)
        await memechan.send(content=message.content, files=files)
        success = 1
    elif str(emoji) == "\N{THUMBS DOWN SIGN}":
        await message.clear_reactions()
        await message.add_reaction(u'\u274C')  # Cross
    else:
        for i in range(0, len(EPISODES)):
            if str(emoji) == u"{}\N{VARIATION SELECTOR-16}\N{COMBINING ENCLOSING KEYCAP}".format(i+1):  # Numbers for episodes
                print("INFO: Meme approved for {}".format(EPISODES[i]))
                memechan = getTextChannelByName("{}-memes".format(EPISODES[i]), guild)
                files = []
                for attachment in message.attachments:
                    fl = await attachment.to_file()
                    files.append(fl)
                await memechan.send(content=message.content, files=files)
                success = 1

    if success:
        await message.clear_reactions()
        await message.add_reaction(u'\u2705')  # Tick
    # After the meme is approved or rejected, the automatically populated reactions are removed
    # If a different reaction is needed, it will require 2 people to add the relevant emoji


async def censorMessageIfPublic(ctx):
    # Returns True if message was censored
    if type(ctx.channel) == discord.TextChannel and (ctx.channel.name in PUBLIC_CHANNELS or ctx.channel.name in ANNOUNCE_CHANNELS):
        # Delete message if sent publically (the announce channels are not sendable, but this also prevents admins doing silly things)
        await ctx.message.delete()

        user = ctx.message.author
        print("Message from {} was censored".format(user.name))
        await user.send(content="Please only use this command in a PM or team chats.")
        return True
    return False


print("Starting Bot...")
bot.run(TOKEN)
