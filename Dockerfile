# syntax=docker/dockerfile:1.1.3-experimental
# Construct a common base image for creating python wheels and the final image
FROM python:3.8.5-alpine3.12@sha256:cbc08bfc4b1b732076742f52852ede090e960ab7470d0a60ee4f964cfa7c710a AS runtime_base

# Setup user
RUN addgroup -g 500 -S hunter2 \
 && adduser -h /opt/hunter2 -s /sbin/nologin -G hunter2 -S -u 500 hunter2
WORKDIR /opt/hunter2/src


# Build image with all the python dependancies.
FROM runtime_base AS python_build

RUN --mount=type=cache,target=/var/cache/apk apk add \
    gcc \
    musl-dev

# Suppress pip version warning, we're keeping the version from the docker base image
ARG PIP_DISABLE_PIP_VERSION_CHECK=1

ENV PATH "/root/.poetry/bin:${PATH}"
ARG poetry_version=1.1.4
RUN wget "https://raw.githubusercontent.com/python-poetry/poetry/${poetry_version}/get-poetry.py" \
 && python get-poetry.py --version "${poetry_version}" \
 && rm get-poetry.py \
 && poetry config virtualenvs.create false \
 && python -m venv /opt/hunter2/venv

ARG dev_flag=" --no-dev"
COPY poetry.lock pyproject.toml /opt/hunter2/src/
RUN --mount=type=cache,target=/root/.cache/pip \
    . /opt/hunter2/venv/bin/activate \
 && poetry install${dev_flag} --no-root


# Build the final image
FROM runtime_base

# Copy in the requried components from the previous build stages
COPY --from=python_build /opt/hunter2/venv /opt/hunter2/venv
COPY . .

USER hunter2

ENTRYPOINT ["/opt/hunter2/venv/bin/python", "bot.py"]
